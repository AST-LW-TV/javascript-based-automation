```

Order of reading 
    +-- sync.js
        +-- async.js
            +-- callbacks.js
                +-- callbackHell.js
                    +-- promise.js
                    +-- callBackRefactorAndAsyncAwait
                        +-- common.js < common file >
                            +-- refactoringCallbackHell.js
                                +-- asyncAwait.js
                                

```

### Commands to run the scripts 

```
    Note: relative to bitOnAsynNature directory
    
    npm run sync
    npm run async
    npm run callback
    npm run callback-hell
    npm run promise
    npm run refactored
    npm run async-await


```