// Now, suppose we want to write the above in Synchronous form then following syntax is used ...

const {employeeInfoAPI, employeeAddressAPI, employeeGiftDistributionAPI} = require("./common");

async function syncManner() {
    try {
        const employeeDetails = await employeeInfoAPI();
        const employeeAddress = await employeeAddressAPI(employeeDetails.employeeName);
        const employeeGift = await employeeGiftDistributionAPI(employeeAddress.address);
        console.log(employeeGift.gift);
    } catch (error) {
        console.log(error);
    }
}

syncManner();

// await operator is used to wait for the promise
// Error handling is done using try-catch block in async functions
