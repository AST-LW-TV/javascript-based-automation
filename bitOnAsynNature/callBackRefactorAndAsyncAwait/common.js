exports.employeeInfoAPI = () => {
    return new Promise((resolve, reject) => {
        console.log("+++++ Calling first callback +++++");
        setTimeout(() => {
            resolve({ id: 1, employeeName: "abc" });
        }, 3000);
    });
};

exports.employeeAddressAPI = (employeeName) => {
    return new Promise((resolve, reject) => {
        console.log("+++++ Calling second callback +++++");
        setTimeout(() => {
            resolve({ address: "123 London road - 000 000" });
        }, 3000);
    });
};

exports.employeeGiftDistributionAPI = (address) => {
    return new Promise((resolve, reject) => {
        console.log("+++++ Calling third callback +++++");
        setTimeout(() => {
            resolve({ gift: "Amazon voucher" });
        }, 3000);
    });
};
