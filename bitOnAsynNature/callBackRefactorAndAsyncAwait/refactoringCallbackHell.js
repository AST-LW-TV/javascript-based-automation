// Refactoring the before code which had callback hell...

const {employeeInfoAPI, employeeAddressAPI, employeeGiftDistributionAPI} = require("./common");

employeeInfoAPI()
    .then((details) => employeeAddressAPI(details.employeeName))
    .then((employeeAddress) => employeeGiftDistributionAPI(employeeAddress.address))
    .then((employeeGift) => console.log(employeeGift.gift));

// The above structure is much much better than call back hell right ?
// simple to understand and easy to implement ...

exports.employeeInfoAPI = employeeInfoAPI;
exports.employeeAddressAPI = employeeAddressAPI; 
exports.employeeGiftDistributionAPI = employeeGiftDistributionAPI;