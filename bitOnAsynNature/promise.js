// Promises - A promise is an object that may produce a single value some time in the future ( found in medium website )
// Each promise have two states to return either "resolved state" or "rejected state"

// Example snippets:

const promise = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve(`+++++ Resolved promise +++++`);
    }, 3000);
});

promise.then((str) => {
    console.log(str);
});

// returning the promises from the function

const thenCatch = (boolValue) => {
    if (boolValue)
        return new Promise((resolve, reject) => {
            resolve(`+++++ Resolved promise +++++`);
        });
    else
        return new Promise((resolve, reject) => {
            reject(new Error("Rejected promise"));
        });
};

// resolved promise
thenCatch(true)
    .then((str) => {
        console.log(str);
    })
    .catch((error) => {
        console.log(error.message);
    });

// rejected promise
thenCatch(false)
    .then((str) => {
        console.log(str);
    })
    .catch((error) => {
        console.log(error.message);
    });