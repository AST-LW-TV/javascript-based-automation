// Asynchronous - Let's understand the following with the help of the example,
// Consider, an API to be used in the current file.
// This API takes around 2 - 3 sec to return the calculated result from the backend when requested
// We cannot make the application to wait for the result to be returned else results in performance issues
// Hence, once the request is given to the backend then we continue with the execution

// The below snippet shows the Asynchronous behaviour

console.log("+++++ Asynchronous behaviour +++++");

const foo = () => {
    setTimeout(() => {
        console.log("Returns result after 3 secs");
    }, 3000);
};

console.log("Start the Execution");
foo();
console.log("End the Execution\n");

// Output:

// +++++ Asynchronous behaviour +++++
// Start the Execution
// End the Execution
// Returns result after 3 secs

// In the above output, we can see that both the consoles statements are executed first and then foo()
