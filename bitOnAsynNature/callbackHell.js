// Nested callbacks - where the next results depends on previous results

// Let's consider an employee API, as shown below

const employeeInfoAPI = (callback) => {
    console.log("+++++ Calling first callback +++++")
    // we will simulate backend behaviour by adding time constraint
    setTimeout(() => {
        callback({ id:1, employeeName: "abc"});
    }, 3000);
};

const employeeAddressAPI = (employeeName, callback) => {
    console.log("+++++ Calling second callback +++++");
    setTimeout(() => {
        callback({ address: "123 London road - 000 000" });
    }, 3000);
};

const employeeGiftDistributionAPI = (address, callback) => {
    console.log("+++++ Calling third callback +++++");
    setTimeout(() => {
        callback({ gift: "Amazon voucher" });
    }, 3000);
};

const employee = employeeInfoAPI((details) => {
    console.log(
        `Employee id: ${details.employeeName} name: ${details.employeeName}`
    );
    const employeeAddress = employeeAddressAPI(
        details.employeeName,
        (employeeAddress) => {
            console.log(
                `${details.employeeName}'s address is ${employeeAddress.address}`
            );
            const employeeGift = employeeGiftDistributionAPI(
                employeeAddress.address,
                (employeeGift) => {
                    console.log(`Employee's gift: ${employeeGift.gift}`);

                    // another callback ... this is an anti-pattern
                }
            );
        }
    );
});

// Each inner callback function takes depends on the result of the previous callback ...
// This leads to callback hell
// In order to avoid this anti-pattern we make use of "promises"
