// Callback - A function invoked by the outer function when passed as an argument to it

// Code snippet to understand the callbacks

const innerFunction = () => {
    console.log("Inner function is invoked by outer function");
};

const outerFunction = (callback) => {
    callback();
};

outerFunction(innerFunction);

// Output:

// Inner function is invoked by outer function
