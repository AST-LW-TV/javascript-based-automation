// Synchronous - code execution line by line ( each line waits for the previous line of code to be executed completely )

// Consider the following snippet

console.log("+++++ Synchronous behaviour +++++");

const foo = () => {
    console.log("Executing the logic inside the function");
};

console.log("Start the execution");
foo();
console.log("End the execution\n");

// Output:

// +++++ Synchronous behaviour +++++
// Start the execution
// Executing the logic inside the function
// End the execution

// In the above we can observe that the code is executed linearly
