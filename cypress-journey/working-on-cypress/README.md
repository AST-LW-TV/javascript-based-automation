### Project Structure

```

    +-- cypress
        +-- assets
            < All downloadable files during execution or files created while test execution can be stored here >
        +-- config
            +-- cypress.api.json
            +-- cypress.db.json
            +-- cypress.ui.json
        +-- fixtures
            +-- api
                < api data required for test execution >
            +-- db
                < db data required for test execution >
            +-- ui
                < ui data required for test execution >
        +-- integration
            +-- api
                < All the API test scripts >
            +-- db
                < All the DB test scripts >
            +-- ui
                < All the UI test scripts  >
            +-- common
                < Some concepts related to Cypress and corresponding tests >
        +-- plugins
            +-- index.js
        +-- support
            +-- api-commons
                < Common functionalities across API test scripts >
            +-- db-commons
                < Common functionalities across DB test scripts >
            +-- ui-commons
                < Common functionalities across UI test scripts >
            +-- index.js
    +-- .gitignore
    +-- cypress.json
    +-- package.json
    +-- README.md


```

### Contents

-   UI Automation Examples
-   API Automation Examples
-   DB Automation Examples

### [UI Automation](https://gitlab.com/AST-LW-TV/javascript-based-automation/-/tree/main/cypress-journey/working-on-cypress/cypress/integration/ui)

#### Problem statements covered

-   Registering of the new customer ( [Website](http://automationpractice.com/index.php) )
-   Capturing all the Cypress issues in JSON file ( [Website](https://github.com/cypress-io/cypress) )
-   Automating some common actions in web like mouse events, date-picker, slider etc. ( [Website](https://jqueryui.com/) )

##### Commad to run in Cypress runner

```
npm run cypress-open:ui
```

##### Command to run in CLI

```
npm run cypress-run:ui
```

### [API Automation](https://gitlab.com/AST-LW-TV/javascript-based-automation/-/tree/main/cypress-journey/working-on-cypress/cypress/integration/api)

Used TMDB API ( [Website](https://www.themoviedb.org/) )

The following APIs are automated,

-   Creating a Guest Session
-   Token Authentication
-   Creating a Session ( ! Guest session)
-   Deleting a Created Session
-   Getting Movies list

Note: In order to work with this APIs, we require api_key [ Credentials of mine are added for understanding of flow ]

##### Commad to run in Cypress runner

```
npm run cypress-open:api
```

##### Command to run in CLI

```
npm run cypress-run:api
```

### [DB Automation](https://gitlab.com/AST-LW-TV/javascript-based-automation/-/tree/main/cypress-journey/working-on-cypress/cypress/integration/db)

Used MongoDB to understand how databases can be automated within Cypress

Test scripts validate the following,

-   Database Validation
-   Creating the collection
-   Droping the collection

Note: Credentials of mine are used to showcase

##### Commad to run in Cypress runner

```
npm run cypress-open:db
```

##### Command to run in CLI

```
npm run cypress-run:db
```

### Some Common Concepts in Cypress

1. Generalized Custom Commands for [Assertions](https://gitlab.com/AST-LW-TV/javascript-based-automation/-/blob/main/cypress-journey/working-on-cypress/cypress/support/commons/assertionsCustomCommands.js)
2. Custom Commands for [Actions](https://gitlab.com/AST-LW-TV/javascript-based-automation/-/blob/main/cypress-journey/working-on-cypress/cypress/support/commons/generalizedCustomCommands.js) that are most frequently used
3. Using of [cy.intercept()](https://gitlab.com/AST-LW-TV/javascript-based-automation/-/blob/main/cypress-journey/working-on-cypress/cypress/integration/common/intercept.spec.js)

### Additional Information

-   All the Configurations for each UI, API and DB are created in [config](https://gitlab.com/AST-LW-TV/javascript-based-automation/-/tree/main/cypress-journey/working-on-cypress/cypress/config) directory
-   All the data is driven from fixtures directory, [UI](https://gitlab.com/AST-LW-TV/javascript-based-automation/-/tree/main/cypress-journey/working-on-cypress/cypress/fixtures/ui), [API](https://gitlab.com/AST-LW-TV/javascript-based-automation/-/tree/main/cypress-journey/working-on-cypress/cypress/fixtures/api), [DB](https://gitlab.com/AST-LW-TV/javascript-based-automation/-/tree/main/cypress-journey/working-on-cypress/cypress/fixtures/db)
-   Custom Commands for [UI](https://gitlab.com/AST-LW-TV/javascript-based-automation/-/tree/main/cypress-journey/working-on-cypress/cypress/support/ui-commons/page-objects) and [DB](https://gitlab.com/AST-LW-TV/javascript-based-automation/-/tree/main/cypress-journey/working-on-cypress/cypress/support/db-commons)
-   All the [cy.task()](https://gitlab.com/AST-LW-TV/javascript-based-automation/-/blob/main/cypress-journey/working-on-cypress/cypress/plugins/index.js) commands are added in plugins/index.js file
-   Concept of [Web Storages](https://gitlab.com/AST-LW-TV/javascript-based-automation/-/blob/main/cypress-journey/working-on-cypress/cypress/integration/common/cookies.spec.js) in browser

### Patterns followed

-   Page-Object Model
