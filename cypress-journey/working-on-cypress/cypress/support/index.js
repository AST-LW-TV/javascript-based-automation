import "./commons/assertionsCustomCommands";
import "./commons/generalizedCustomCommands";
import "./db-commons/dbCommands";
import "./db-commons/custom-commands";
import "./ui-commons/commons/custom-commands"
import "cypress-iframe";

require("cypress-downloadfile/lib/downloadFileCommand");

const assertArrays = require("chai-arrays");
chai.use(assertArrays);

const hideXhrLogs = require("./commons/hideXhrLogs");
hideXhrLogs.hideXhrLogs();

Cypress.on("uncaught:exception", (err, runnable) => {
    return false;
});
