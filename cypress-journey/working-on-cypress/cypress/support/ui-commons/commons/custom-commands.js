import authenticationPage from "../page-objects/automation-practice-website/authenticationPage";
import homePage from "../page-objects/automation-practice-website/homePage";
import loggedInPage from "../page-objects/automation-practice-website/loggedInPage";
import orderPage from "../page-objects/automation-practice-website/orderPage";

// only common functionalities are declared in custom-commands file
// actions for a particular page should be declared in that page itself

// some times if tests use different uri's within the same suite then we can define the most general one in custom-commands
Cypress.Commands.add("navigateToHomePage", () => {
    cy.visit("http://automationpractice.com/index.php");
});

Cypress.Commands.add(
    "enterTheCredentialsAndVerify",
    (page, data, key, expectedMsg) => {
        // the use of for loop is that when wrong credentials are entered then the page is not navigated
        // instead displays the error message
        // hence we can reuse code by just clearing and re-typing the credentials
        // each iteration takes a separate set
        page.data = data;
        for (let i = 1; i <= 3; i++) {
            const email =
                data["authenticate_newuser_data"][key][`set${i}`]["email"];
            const password =
                data["authenticate_newuser_data"][key][`set${i}`]["password"];
            page.enterRegisteredUserDetails(email, password);
            page.clickOnSubmitButton();
            page.getErrorMsg();
            cy.get("@msg").then((msg) => {
                expect(msg.trim()).to.include(expectedMsg[i - 1]);
            });
        }
    }
);

Cypress.Commands.add("logInTheUser", (data) => {
    // visiting the authentication page, as we option of visiting without any restrictions or pre-requisites
    cy.visit(
        "http://automationpractice.com/index.php?controller=authentication"
    );
    const email = data["common_data"]["registered_user_data"]["email"];
    const password = data["common_data"]["registered_user_data"]["password"];
    authenticationPage.enterRegisteredUserDetails(email, password);
    authenticationPage.clickOnSubmitButton();
});

// we are depending on the change in the width when next or prev buttons pressed to track our test
// the below code behaves in flaky manner when removed the code duplication
// needs further maintains...
Cypress.Commands.add("clickButtonAndVerify", (locator, nextOrPrev) => {
    // the use of explicit waits is to wait for the image to completely occupy the specified region
    // meaning if no wait is used then, image only covers 20% of the specified width out of 100%
    let beforePosition = null;
    cy.get(homePage.getCarouselComponent.sliderParentLocator).trigger(
        "mouseover"
    );

    if (nextOrPrev) {
        cy.get(homePage.getCarouselComponent.sliderLocator).then(
            ($document) => {
                cy.wrap(Math.abs(parseInt($document.css("left"))))
                    .as("imagePosition")
                    .then((value) => {
                        beforePosition = value;
                    });
            }
        );
    } else {
        // in case of testing previous button, first we click on it and then we get the position value
        // then compare it to be less than the reference value
        cy.get(locator).click();
        cy.wait(1000);
        cy.get(homePage.getCarouselComponent.sliderLocator).then(
            ($document) => {
                cy.wrap(Math.abs(parseInt($document.css("left"))))
                    .as("imagePosition")
                    .then((value) => {
                        beforePosition = value;
                    });
            }
        );
    }

    function repeat() {
        cy.get(locator).click();
        cy.wait(1000);

        cy.get(homePage.getCarouselComponent.sliderLocator).then(
            ($document) => {
                cy.wrap(Math.abs(parseInt($document.css("left"))))
                    .as("imagePosition")
                    .then(() => {
                        cy.get("@imagePosition").then((value) => {
                            if (nextOrPrev) {
                                expect(beforePosition).to.be.lessThan(value);
                                beforePosition = value;
                            } else {
                                expect(beforePosition).to.be.greaterThan(value);
                                beforePosition = value;
                            }
                        });
                    });
            }
        );
    }

    repeat();
    repeat();
});

Cypress.Commands.add("hoverVerifyMoveOut", () => {
    // the reason of using viewport is because the following test is specific to width size
    // by default the width of cypress is 600
    // the following dimensions are of mac book pro
    const width = 2560;
    const height = 1600;
    // actions
    cy.viewport(width, height);
    cy.get(homePage.getProductContainerComponent.productContainer)
        .find("li")
        .each(($domElement) => {
            cy.wrap($domElement)
                .trigger("mouseover")
                .find(homePage.getProductContainerComponent.buttonContainer)
                .then(($domElement) => {
                    const display = $domElement.css("display");
                    // asserting and continue action
                    expect(display).to.be.equal("block");
                })
                .trigger("mouseout");
        });
});

Cypress.Commands.add("clickOnProductAndWaitForPopup", (productId) => {
    cy.get(homePage.getProductContainerComponent.buttonContainer)
        .eq(productId)
        .find(homePage.getProductContainerComponent.addToCartLocater)
        .click({ force: true });
    // guard - that is used to wait until the layer cart popup become visible
    cy.get(homePage.getLayerCartComponent.layerCartLocator).should(
        "have.css",
        "display",
        "block"
    );
});

Cypress.Commands.add("addToCart", (data, temp) => {
    // after authentication, we should navigate to home page
    // if temp is false, it indicates that the page is already present in home page
    // so no need to navigate to home page ...
    if (temp) {
        cy.logInTheUser(data);
        loggedInPage.clickOnLogo();
    }
    homePage.addToCart(data["general_functionalities_data"]["product_id"]);
});

Cypress.Commands.add("deleteTheProductFromCart", () => {
    // guard assertion
    cy.get(homePage.getCartComponent.cartLocator)
        .trigger("mouseover")
        .get(homePage.getCartComponent.cartBlockLocator)
        .should("have.css", "display", "block");
    cy.get(homePage.getCartComponent.deleteProductFromCartLocator).click();
});

Cypress.Commands.add("orderProduct", () => {
    cy.get("@data").then((data) => {
        const productId = data["general_functionalities_data"]["product_id"];
        // actions
        cy.logInTheUser(data);
        loggedInPage.clickOnLogo();
        cy.clickOnProductAndWaitForPopup(productId).then(() => {
            cy.get(
                homePage.getLayerCartComponent.proceedToCheckoutLocator
            ).click();
        });
        orderPage.clickOnProceedToCheckoutButton();
        orderPage.clickOnAddressConfirmButton();
        orderPage.checkTheTermsOfService();
        orderPage.clickOnCarrierConfirmButton();
        orderPage.payBy("bankWire");
        orderPage.clickOnConfirmOrderButton();
    });
});

Cypress.Commands.add("navigateToOrderDetailsPage", (data) => {
    cy.get("@data").then((data) => {
        cy.logInTheUser(data);
        loggedInPage.clickOnOrderAndHistoryDetailsLink();
    });
});
