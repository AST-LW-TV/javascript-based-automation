// general fs module does not with browser based applications 
// so we alternate module called browserify-js 
// official statement from npm - "fs for the browser using level-filesystem and browserify"
const fs = require("browserify-fs"); 

class HomePage {
    static clickOnIssuesLink() {
        cy.get("a[id='issues-tab']").click();
    }

    static captureAllIssuesAndNavigateToNextPage() {
        function clickOnNextButton() {
            cy.get(".next_page").then(($domElement) => {
                cy.get("a[id^='issue_']").each(($domElement) => {
                    const innerText = $domElement.text();
                    const link = $domElement.prop("href");
                    HomePage.issues.push({ [innerText]: link });
                });
                if (
                    !$domElement.prop("class").split(" ").includes("disabled")
                ) {
                    cy.get(".next_page").eq(0).click();
                    // encountered -  You have triggered an abuse detection mechanism.
                    // this is emitted by github website coz traversal speed is very high and causes some security issues
                    cy.wait(3000);
                    clickOnNextButton();
                }
            });
        }
        clickOnNextButton();
    }

    static saveTheResultsInAssets() {
        function createFile(contents) {
            contents = contents
                .map((element) => {
                    return JSON.stringify(element);
                })
                .toString();
            fs.writeFile("../../assets/users.json", contents, function (err) {
                if (err) throw err;
            });
        }

        cy.task("createFile", HomePage.issues).then((value) => {
            expect(value).to.be.true;
        });
    }
}

HomePage.issues = [];

module.exports.HomePage = HomePage;
