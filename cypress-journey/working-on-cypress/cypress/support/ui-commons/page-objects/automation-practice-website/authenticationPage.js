class AuthenticationPage {
    // components within the page that have locators in them
    _notRegisteredComponent = {
        emailLocator: "#email_create",
        createAnAccountLocator: "#SubmitCreate",
    };

    _alreadyRegisteredComponent = {
        emailLocator: "#email",
        passwordLocator: "#passwd",
        submitButtonLocator: "#SubmitLogin",
    };

    _createAnAccountFormComponent = {
        formLocator: "#account-creation_form",
        genderSelectorLocator: `#uniform-id_gender`,
        firstNameLocator: "#customer_firstname",
        lastNameLocator: "#customer_lastname",
        emailLocator: "#email",
        passwordLocator: "#passwd",
        dayLocator: "#days",
        monthLocator: "#months",
        yearLocator: "#years",
        newsletterLocator: "#newsletter",
        specialOffersLocator: "#optin",
        reenterFirstNameLocator: "#firstname",
        reenterLastNameLocator: "#lastname",
        companyNameLocator: "#company",
        address1Locator: "#address1",
        address2Locator: "#address2",
        cityLocator: "#city",
        stateLocator: "#id_state",
        postCodeLocator: "#postcode",
        countryLocator: "#id_country",
        additionalInfoLocator: "#other",
        phoneNumberLocator: "#phone",
        mobileNumberLocator: "#phone_mobile",
        aliasAddressLocator: "#alias",
        registerText: "Register",
    };

    _authenticationComponent = {
        errorMsgLocator: ".alert.alert-danger li",
    };

    constructor(data) {
        this.data = data;
    }

    enterEmailAddress(parentKey, childKey) {
        cy.get(this._notRegisteredComponent.emailLocator).type(
            this.data[parentKey][childKey]["email"]
        );
        cy.get(this._notRegisteredComponent.createAnAccountLocator).click();
    }

    enterDetailsOfCustomer(parentKey, childKey) {
        cy.get(this._createAnAccountFormComponent.formLocator).then(
            ($domElement) => {
                cy.wrap($domElement).as("form");
                const genderSelector =
                    this._createAnAccountFormComponent.genderSelectorLocator +
                    `${this.data[parentKey][childKey]["title"]}`;
                cy.get("@form").find(genderSelector).click();
                cy.get("@form")
                    .find(this._createAnAccountFormComponent.firstNameLocator)
                    .type(
                        this.data[parentKey][childKey][
                            "first-name"
                        ]
                    );
                cy.get("@form")
                    .find(this._createAnAccountFormComponent.lastNameLocator)
                    .type(
                        this.data[parentKey][childKey][
                            "last-name"
                        ]
                    );
                cy.get("@form")
                    .find(this._createAnAccountFormComponent.emailLocator)
                    .clear()
                    .type(
                        this.data[parentKey][childKey][
                            "email"
                        ]
                    );
                cy.get("@form")
                    .find(this._createAnAccountFormComponent.passwordLocator)
                    .type(
                        this.data[parentKey][childKey][
                            "password"
                        ]
                    );
                cy.get("@form").then(($domElement) => {
                    const [day, month, year] =
                        this.data[parentKey][childKey][
                            "date-of-birth"
                        ].split("-");
                    cy.wrap($domElement).as("dateOfBirth");
                    cy.get("@dateOfBirth")
                        .find(this._createAnAccountFormComponent.dayLocator)
                        .select(day);
                    cy.get("@dateOfBirth")
                        .find(this._createAnAccountFormComponent.monthLocator)
                        .select(month);
                    cy.get("@dateOfBirth")
                        .find(this._createAnAccountFormComponent.yearLocator)
                        .select(year);
                });
                cy.get("@form").then(($domElement) => {
                    cy.wrap($domElement).as("checkbox");
                    cy.get("@checkbox")
                        .find(
                            this._createAnAccountFormComponent.newsletterLocator
                        )
                        .check();
                    cy.get("@checkbox")
                        .find(
                            this._createAnAccountFormComponent
                                .specialOffersLocator
                        )
                        .check();
                });
                cy.get("@form")
                    .find(
                        this._createAnAccountFormComponent
                            .reenterFirstNameLocator
                    )
                    .type(
                        this.data[parentKey][childKey][
                            "first-name"
                        ]
                    );
                cy.get("@form")
                    .find(
                        this._createAnAccountFormComponent
                            .reenterLastNameLocator
                    )
                    .type(
                        this.data[parentKey][childKey][
                            "last-name"
                        ]
                    );
                cy.get("@form")
                    .find(this._createAnAccountFormComponent.companyNameLocator)
                    .type(
                        this.data[parentKey][childKey][
                            "company"
                        ]
                    );
                cy.get("@form")
                    .find(this._createAnAccountFormComponent.address1Locator)
                    .type(
                        this.data[parentKey][childKey][
                            "address1"
                        ]
                    );
                cy.get("@form")
                    .find(this._createAnAccountFormComponent.address2Locator)
                    .type(
                        this.data[parentKey][childKey][
                            "address2"
                        ]
                    );
                cy.get("@form")
                    .find(this._createAnAccountFormComponent.cityLocator)
                    .type(
                        this.data[parentKey][childKey][
                            "city"
                        ]
                    );
                cy.get("@form")
                    .find(this._createAnAccountFormComponent.stateLocator)
                    .as("selectState");
                cy.get("@selectState")
                    .children()
                    .each(($domElement) => {
                        if (
                            $domElement.text().toLowerCase() ===
                            this.data[parentKey][childKey][
                                "state"
                            ]
                        ) {
                            const value = $domElement.prop("value");
                            cy.get("@selectState").select(value);
                            return;
                        }
                    });
                cy.get("@form")
                    .find(this._createAnAccountFormComponent.postCodeLocator)
                    .type(
                        this.data[parentKey][childKey][
                            "zip-code"
                        ]
                    );
                cy.get("@form")
                    .find(this._createAnAccountFormComponent.countryLocator)
                    .as("selectCountry");
                cy.get("@selectState")
                    .children()
                    .each(($domElement) => {
                        if (
                            $domElement.text().toLowerCase() ===
                            this.data[parentKey][childKey][
                                "country"
                            ]
                        ) {
                            const value = $domElement.prop("value");
                            cy.get("@selectCountry").select(value);
                            return;
                        }
                    });
                cy.get("@form")
                    .find(
                        this._createAnAccountFormComponent.additionalInfoLocator
                    )
                    .type(
                        this.data[parentKey][childKey][
                            "additional-information"
                        ]
                    );
                cy.get("@form")
                    .find(this._createAnAccountFormComponent.phoneNumberLocator)
                    .type(
                        this.data[parentKey][childKey][
                            "home-phone"
                        ]
                    );
                cy.get("@form")
                    .find(
                        this._createAnAccountFormComponent.mobileNumberLocator
                    )
                    .type(
                        this.data[parentKey][childKey][
                            "mobile-phone"
                        ]
                    );
                cy.get("@form")
                    .find(
                        this._createAnAccountFormComponent.aliasAddressLocator
                    )
                    .clear()
                    .type(
                        this.data[parentKey][childKey][
                            "alias-address"
                        ]
                    );
            }
        );
    }

    clickOnRegisterButton() {
        cy.contains(this._createAnAccountFormComponent.registerText).click();
    }

    enterRegisteredUserDetails(email, password) {
        cy.get(this._alreadyRegisteredComponent.emailLocator)
            .clear()
            .type(email);
        cy.get(this._alreadyRegisteredComponent.passwordLocator)
            .clear()
            .type(password);
    }

    clickOnSubmitButton() {
        cy.get(this._alreadyRegisteredComponent.submitButtonLocator).click();
    }

    // Cypress commands are queued before running / executing, hence in order to return a value
    // from the method we make use of alias
    getErrorMsg() {
        cy.get(this._authenticationComponent.errorMsgLocator).then(
            ($domElement) => {
                cy.wrap($domElement.text()).as("msg");
            }
        );
    }
}

AuthenticationPage.data = null;

export default new AuthenticationPage();
