class LoggedInPage {
    _headerComponent = {
        logoLocator: "a[title='My Store']",
    };

    _myAccountComponent = {
        ulLocator: ".myaccount-link-list",
        homePageLinkLocator: "a[title='Home']",
        listOfForwardLinkLocators: [
            "a[title='Orders']",
            "a[title='Credit slips']",
            "a[title='Addresses']",
            "a[title='Information']",
            "a[title='My wishlists']",
        ],
    };

    captureDisplayedTexts() {
        const temp = []; // to store the text from the li fields
        cy.get(this._myAccountComponent.ulLocator)
            .find("li")
            .each(($domElement) => {
                temp.push($domElement.text());
            });
        cy.wrap(temp).as("displayedTexts");
    }

    clickOnHomeButton() {
        cy.get(this._myAccountComponent.homePageLinkLocator).click();
    }

    verifyNavigationOfAllFieldsInLoggedPage(data) {
        const temp = data["navigations_data"]["url_partial_link_text"];
        for (let i = 0; i < temp.length; i++) {
            cy.get(
                this._myAccountComponent.listOfForwardLinkLocators[i]
            ).click();
            cy.url().should("contain", temp[i]);
            cy.go("back");
        }
    }

    clickOnLogo() {
        cy.get(this._headerComponent.logoLocator).click();
    }

    clickOnOrderAndHistoryDetailsLink() {
        cy.get(this._myAccountComponent.listOfForwardLinkLocators[0]).click();
    }
}

export default new LoggedInPage();
