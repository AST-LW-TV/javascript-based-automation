class CategoriesPage {
    _pageBarComponent = {
        gridLocator: "#grid",
        listLocator: "#list",
    };

    get getPageBarComponent() {
        return this._pageBarComponent;
    }

    clickOnGrid() {
        cy.get(this._pageBarComponent.gridLocator).click();
    }

    clickOnList() {
        cy.get(this._pageBarComponent.listLocator).click();
    }
}

export default new CategoriesPage();
