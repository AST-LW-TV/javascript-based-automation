import homePage from "./homePage";

class ProductPage {
    _containerComponent = {
        headersLocators: ".page-product-heading",
    };

    // this headers are only present in product page
    returnTheHeaders() {
        const temp = [];
        cy.get(this._containerComponent.headersLocators).each(($domElement) => {
            temp.push($domElement.text().trim().toLowerCase());
        });
        cy.wrap(temp).as("listOfHeaders");
    }
}

export default new ProductPage();
