const path = require("path");

class OrderAndDetailsPage {
    _orderHistoryComponent = {
        historyInvoiceLinkLocators: ".history_invoice > .link-button",
        detailButtonLocators: ".history_detail :nth-child(1)",
        reorderLocators: ".history_detail :nth-child(2)",
    };

    _messageComponent = {
        selectProductLocator: "select[name='id_product']",
        commentLocator: "textarea[name='msgText']",
        sendButtonLocator: "button[name='submitMessage']",
        successMsgLocator: ".alert-success",
    };

    get getMessageComponent() {
        return this._messageComponent;
    }

    clickOnTheReorderButton() {
        cy.get(this._orderHistoryComponent.reorderLocators)
            .eq(0)
            .click({ force: true });
    }

    clickOnTheDetailButton() {
        cy.get(this._orderHistoryComponent.detailButtonLocators)
            .eq(0)
            .click({ force: true });
    }

    downloadLatestInvoice() {
        cy.get(this._orderHistoryComponent.historyInvoiceLinkLocators)
            .eq(0)
            .then(($domElement) => {
                const downloadLink = $domElement.attr("href");
                cy.downloadFile(downloadLink, "downloads", "invoice.pdf");
            })
            .then(() => {
                const downloadFilePath = path.join(
                    "/Users/ast-lw/Desktop/TestVagrant/git-repos-I-am-working/javascript-based-automation/cypress-journey/working-on-cypress/downloads",
                    "invoice.pdf"
                );
                cy.readFile(downloadFilePath, "binary").then((file) => {
                    expect(file.length).to.be.greaterThan(0);
                });
            });
    }

    addCommentOrMessage() {
        cy.get("@data").then((data) => {
            cy.get(this._messageComponent.selectProductLocator).select("1");
            cy.get(this._messageComponent.commentLocator).invoke(
                "val",
                data["general_functionalities_data"]["comment"]
            );
        });
    }

    clickOnSendButton() {
        cy.get(this._messageComponent.sendButtonLocator).click();
    }
}

export default new OrderAndDetailsPage();
