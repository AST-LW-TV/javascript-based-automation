class OrderPage {
    _commonComponents = {
        proceedToCheckoutLocator:
            "#center_column [title='Proceed to checkout']",
        addressConfirmLocator: "button[name='processAddress']",
        carrierConfirmLocator: "button[name='processCarrier']",
        checkboxLocator: "#uniform-cgv",
        paymentLocator: ".payment_module",
        confirmOrderLocator: "#cart_navigation [type]",
        successMsgLocator: ".cheque-indent",
        userLocator: "a[title='View my customer account']",
    };

    clickOnProceedToCheckoutButton() {
        cy.get(this._commonComponents.proceedToCheckoutLocator).click();
    }

    clickOnAddressConfirmButton() {
        cy.get(this._commonComponents.addressConfirmLocator).click();
    }

    checkTheTermsOfService() {
        cy.get(this._commonComponents.checkboxLocator).click();
    }

    clickOnCarrierConfirmButton() {
        cy.get(this._commonComponents.carrierConfirmLocator).click();
    }

    payBy(method) {
        const temp = {
            bankWire: 0,
            check: 1,
        };
        cy.get(this._commonComponents.paymentLocator).eq(temp[method]).click();
    }

    clickOnConfirmOrderButton() {
        cy.get(this._commonComponents.confirmOrderLocator).click();
    }

    getSuccessMsg() {
        cy.get(this._commonComponents.successMsgLocator)
            .invoke("text")
            .as("successMsg");
    }

    navigateToUserPage() {
        cy.get(this._commonComponents.userLocator).click();
    }
}

export default new OrderPage();
