import authenticationPage from "../../page-objects/automation-practice-website/authenticationPage";
import productPage from "./productPage";

class HomePage {
    _navigationBarComponent = {
        signInText: "Sign in",
    };

    _categoriesComponent = {
        womenCategory: "a[title='Women']",
        dressesCategory:
            "#block_top_menu > ul:nth-child(2) > li:nth-of-type(2) .sf-with-ul",
        tshirtsCategory: "#block_top_menu li:nth-of-type(3) [title='T-shirts']",
    };

    _carouselsComponent = {
        sliderParentLocator: "#homepage-slider",
        sliderLocator: "#homeslider",
        staticImageLocator:
            ".homeslider-container.bx-clone a[title='sample-1']",
    };

    _productContainerComponent = {
        productContainer: "#homefeatured",
        buttonContainer: ".button-container",
        addToCartLocater: "a[title='Add to cart']",
    };

    _layerCartComponent = {
        layerCartLocator: "#layer_cart",
        layerCartProductLocator: ".layer_cart_product",
        closeButtonLocator: "span[title='Close window']",
        continueShoppingLocator: "span[title='Continue shopping']",
        proceedToCheckoutLocator: "a[title='Proceed to checkout']",
    };

    _cartComponent = {
        cartLocator: "a[title='View my shopping cart']",
        cartBlockLocator: ".cart_block",
        totalPriceLocator: ".cart-info > span",
        deleteProductFromCartLocator: "span[class='remove_link']",
        cartBlockNoProductLocator: ".cart_block_no_products",
    };

    // getters
    get getCategoriesComponent() {
        return this._categoriesComponent;
    }

    get getCarouselComponent() {
        return this._carouselsComponent;
    }

    get getProductContainerComponent() {
        return this._productContainerComponent;
    }

    get getLayerCartComponent() {
        return this._layerCartComponent;
    }

    get getCartComponent() {
        return this._cartComponent;
    }

    clickOnSignInButton() {
        cy.contains(this._navigationBarComponent.signInText).click();
        return authenticationPage;
    }

    verifyTheCarouselsRotation(data) {
        const locator = this._carouselsComponent.sliderLocator;
        function repeat(index) {
            cy.get(locator).then(($domElement) => {
                const range =
                    data["carousels_data"]["positions"][`position${index}`];
                cy.wait(3500); // explicitly waiting for the carousel changing
                const positionValue = Math.abs(
                    parseInt($domElement.css("left"))
                );
                expect(positionValue)
                    .to.be.greaterThan(range[0])
                    .and.to.be.lessThan(range[1]);
            });
        }
        for (let index = 1; index < 4; index++) {
            repeat(index);
        }
    }

    verifyIfImageIsNotChanged() {
        cy.get(this._carouselsComponent.sliderParentLocator).trigger(
            "mouseover"
        );
        cy.wait(10000); // explicitly waiting ... like tester waits manually for the change to appear
        cy.get(this._carouselsComponent.staticImageLocator).then(
            ($domElement) => {
                const titleOfTheImage = $domElement.attr("title");
                expect(titleOfTheImage).not.to.be.equal("sample-2");
            }
        );
    }

    _helper(index, expectedOutcomes) {
        cy.get(this._productContainerComponent.productContainer)
            .children()
            .then(($domElements) => {
                cy.wrap($domElements[index])
                    .find("img")
                    .click()
                    .then(() => {
                        productPage.returnTheHeaders();
                    });
                cy.get("@listOfHeaders").then((actualOutcomes) => {
                    // asserting
                    expect(expectedOutcomes).to.have.members(actualOutcomes);
                });
                cy.navigateToHomePage();
            });
    }

    // cypress throws error of dom elements got detached when we navigate back to home page from product page when used with "find" command
    clickOnEachProductOnHomePageAndVerifyRedirection(expectedOutcomes) {
        let length = null;
        cy.get(this._productContainerComponent.productContainer)
            .children()
            .then(($domElements) => {
                length = $domElements.length;
            })
            .then(() => {
                // change 1 to length to test all the product pages
                // the reason i am limiting to 1 coz, this is stupid website and shows resource overused ....
                // for (let i = 0; i < length; i++) {
                for (let i = 0; i < 1; i++) {
                    this._helper(i, expectedOutcomes);
                }
            });
    }

    clickOnCategoriesAndVerifyTheUrl() {
        cy.get(this._categoriesComponent.womenCategory).click();
        cy.url().should("contain", "id_category=3");
        cy.get(this._categoriesComponent.dressesCategory).click();
        cy.url().should("contain", "id_category=8");
        cy.get(this._categoriesComponent.tshirtsCategory).click();
        cy.url().should("contain", "id_category=5");
    }

    clickOnCategoryLink(locator) {
        cy.get(locator).click();
    }

    addToCart(productId) {
        cy.clickOnProductAndWaitForPopup(productId);
        cy.get(this._layerCartComponent.layerCartProductLocator)
            .find("h2")
            .invoke("text")
            .as("successfulAdditionToCartMsg");
        cy.get(this._layerCartComponent.closeButtonLocator).click();
    }

    totalPrice() {
        cy.get(this._cartComponent.totalPriceLocator)
            .invoke("text")
            .as("totalPrice");
    }
}

export default new HomePage();
