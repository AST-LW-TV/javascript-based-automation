Cypress.Commands.add("dbPresence", (arg, expectedValue) => {
    let isPresent = false;
    for (let item of arg.databases) {
        if (item["name"] === expectedValue) {
            isPresent = true;
            break;
        }
    }
    return isPresent
});
