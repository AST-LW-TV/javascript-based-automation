const mongodb = require("mongodb");

function returnClient(url) {
    return new Promise((resolve, reject) => {
        mongodb.MongoClient.connect(url, (err, client) => {
            resolve(client);
        });
    });
}

exports.dbList = (url) => {
    return new Promise((resolve, reject) => {
        returnClient(url).then((client) => {
            resolve(client.db().admin().listDatabases());
        });
    });
};

exports.dbCreateCollection = ({ url, dbName, collectionName }) => {
    return new Promise((resolve, reject) => {
        returnClient(url).then((client) => {
            client.db(dbName).createCollection(collectionName, (err, res) => {
                if (err) throw err;
                resolve(true);
            });
        });
    });
};

exports.dbDropCollection=({url,dbName,collectionName})=>{
    return new Promise((resolve,reject)=>{
        returnClient(url).then((client)=>{
            client.db(dbName).collection(collectionName).drop((err,res)=>{
                if(err) throw err; 
                resolve(true);
            })
        })
    })
}