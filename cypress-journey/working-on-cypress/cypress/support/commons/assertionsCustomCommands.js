// Note:
//      flag = false is negation

// General validations

// eg: cy.typeValidation("foo","string");
//     cy.typeValidation(undefined,"undefined");

import { stringify, parse } from "flatted";

Cypress.Commands.add(
    "typeValidation",
    (actualValue, expectedType, flag = true) => {
        flag
            ? expect(actualValue).to.be.a(expectedType)
            : expect(actualValue).to.not.be.a(expectedType);
    }
);

// eg: cy.included("hello world","hello");
Cypress.Commands.add("included", (actualValue, expectedValue, flag = true) => {
    flag
        ? expect(actualValue).to.include(expectedValue)
        : expect(actualValue).to.not.include(expectedValue);
});

// eg: cy.equal("foo","foo");
Cypress.Commands.add("equal", (actualValue, expectedValue, flag = true) => {
    flag
        ? expect(actualValue).to.equals(expectedValue)
        : expect(actualValue).to.not.equals(expectedValue);
});

// Rendered UI validations

// arg: data ( it is not a yielded domElement )

Cypress.Commands.add("lengthValidation", (arg, expectedValue, flag = true) => {
    flag
        ? cy.wrap(arg).should("have.length", expectedValue)
        : cy.wrap(arg).should("not.have.length", expectedValue);
});

Cypress.Commands.add("substringPresence", (arg, expectedString) => {
    cy.wrap(arg).should("contains", expectedString);
});

// arg: yielded domElement

// snippet
// cy.get(<selector>).then($domElement=>{
//      < applying the validations by the calling the custom commands >
// })

// need to work on this ...
// Cypress.Commands.add("compoundAssertion", (arg, expectedValue = []) => {
//     console.log(arg);
//     let temp = stringify(arg);
//     let completeStatement = `cy.wrap().should(${expectedValue[0][0]},${expectedValue[0][1]})`;
//     // cy.log(completeStatement);
//     // cy.log(stringify(arg));
//     // cy.log(parse(stringify(arg)));
//     // cy.log(completeStatement);
//     for (let i = 1; i < expectedValue.length; i++) {
//         const stringFormat = `.and(${expectedValue[i][0]},${expectedValue[i][1]})`;
//         completeStatement += stringFormat;
//     }
//     completeStatement += ";";
//     cy.log(completeStatement);
//     eval(
//         `cy.wrap(${arg}).should("have.length",7)`
//     );
// });

Cypress.Commands.add("areEqual", (arg, expectedValue, flag = true) => {
    cy.wrap(arg).then(($domElement) => {
        const text = $domElement.text();
        flag
            ? expect(text).to.equal(expectedValue)
            : expect(text).to.not.equal(expectedValue);
    });
});

Cypress.Commands.add("validateAttributeValue", (arg, key, value) => {
    cy.wrap(arg).should("have.attr", key, value);
});

Cypress.Commands.add("validateCheckedItem", (arg, flag = true) => {
    flag
        ? cy.wrap(arg).should("be.checked")
        : cy.wrap(arg).should("not.be.checked");
});

Cypress.Commands.add(
    "validateCSSProperties",
    (arg, key, value, flag = true) => {
        flag
            ? cy.wrap(arg).should("have.css", key, value)
            : cy.wrap(arg).should("not.have.css", key, value);
    }
);

Cypress.Commands.add(
    "subStringPresenceOnElement",
    (arg, expectedValue, flag = true) => {
        flag
            ? cy.wrap(arg).should("contain", expectedValue)
            : cy.wrap(arg).should("not.contain", expectedValue);
    }
);

Cypress.Commands.add(
    "doesItHaveClassName",
    (arg, expectedValue, flag = true) => {
        flag
            ? cy.wrap(arg).should("have.class", expectedValue)
            : cy.wrap(arg).should("not.have.class", expectedValue);
    }
);

Cypress.Commands.add("validateCount", (arg, expectedValue, flag = true) => {
    flag
        ? cy.wrap(arg).should("have.length", expectedValue)
        : cy.wrap(arg).should("not.have.length", expectedValue);
});

Cypress.Commands.add("isVisible", (arg, flag = true) => {
    flag
        ? cy.wrap(arg).should("be.visible")
        : cy.wrap(arg).should("not.be.visible");
});

Cypress.Commands.add("isSelected", (arg, flag = true) => {
    flag
        ? cy.wrap(arg).should("be.checked")
        : cy.wrap(arg).should("not.be.checked");
});

Cypress.Commands.add("isDisabled", (arg, flag = true) => {
    flag
        ? cy.wrap(arg).should("be.disabled")
        : cy.wrap(arg).should("not.be.disabled");
});

// JSON validations

// validates the complete json object as a whole
Cypress.Commands.add(
    "jsonObjectValidation",
    (actualValue, expectedValue, flag = true) => {
        const statement = flag
            ? `expect(${JSON.stringify(
                  actualValue
              )}).to.deep.equal(${JSON.stringify(expectedValue)});`
            : `expect(${JSON.stringify(
                  actualValue
              )}).to.deep.not.equal(${JSON.stringify(expectedValue)});`;
        eval(String(statement));
        // the above code can also be written in simpler terms by converting to string and comparing
        // the aim of above code snippet is to know how to convert string to statement
    }
);

// validates the value for the particular key in json
// eg: cy.jsonPropertyValidation({ a: { b: 1 } }, "a", { b: 1 });
Cypress.Commands.add(
    "jsonPropertyValidation",
    (actualValue, key = "", expectedValue, flag = true) => {
        const aValue = JSON.stringify(actualValue[key]);
        const eValue = JSON.stringify(expectedValue);
        assert.equal(aValue, eValue);
    }
);
