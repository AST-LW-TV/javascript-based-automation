// arg: is the yielded domElement

// Notes:
// - https://api.jquery.com/category/selectors/ - CSS selectors usage

Cypress.Commands.add("getText", (arg) => {
    cy.wrap(arg).text();
});

Cypress.Commands.add("removeAttribute", (arg, attributeName) => {
    cy.wrap(arg).invoke("removeAttr", attributeName);
});

// selector is CSS selector
// eg: .caption or #contact-us
Cypress.Commands.add("removeDomElement", (arg, selector) => {
    cy.wrap(arg).invoke("remove", selector);
});

// removes all the child dom elements relative the specified parent element
Cypress.Commands.add("removeAllChildDomElements", (arg) => {
    cy.wrap(arg).invoke("empty");
});

Cypress.Commands.add("addClassName", (arg, className) => {
    arg.addClass(className); // operating directly using jQuery method
});

Cypress.Commands.add("removeClassName", (arg, className) => {
    arg.removeClass(className);
});

Cypress.Commands.add("toggleClassName", (arg, className) => {
    arg.toggleClass(className);
});

Cypress.Commands.add("getPropertyValue", (arg, propertyKey) => {
    return arg.prop(propertyKey);
});

Cypress.Commands.add("setPropertyValue", (arg, propertyKey, valueToSet) => {
    arg.prop(propertyKey, valueToSet);
});

Cypress.Commands.add("removeProperty", (arg, propertyKey) => {
    arg.removeProp(propertyKey);
});

// returning CSS dimensions on property
Cypress.Commands.add("getDimensions", (arg) => {
    return {
        position: arg.offset(),
        height: arg.height(),
        width: arg.width(),
        innerHeight: arg.innerHeight(),
        innerWidth: arg.innerWidth(),
        outerHeight: arg.outerHeight(),
        outerWidth: arg.outerWidth(),
    };
});

// hide or show or toggle the dom elements
Cypress.Commands.add("effects", (arg, type) => {
    switch (type) {
        case "hide":
            arg.hide(); // hide the dom element
            break;
        case "show":
            arg.show(); // show the hidden dom element
            break;
        case "toggle":
            arg.toggle(); // toggle between hidden or expose the dom element
            break;
        default:
            break;
    }
});

// scrolls to desired position, where position is mentioned in pixels
Cypress.Commands.add("scrollToDesiredPosition", (arg) => {
    cy.getDimensions(arg).then((dimension) => {
        const position = dimension.position;
        cy.scrollTo(position.left, position.top);
    });
});

// complex mouse events
Cypress.Commands.add("mouseEvent", (arg, eventName) => {
    switch (eventName) {
        case "click":
            arg.click();
            break;
        case "doubleClick":
            arg.dblclick();
            break;
        // need to identify the different events
        default:
            break;
    }
});

// in this custom command we just pass the selector and value to be selected
// and it selects that value
Cypress.Commands.add("selectOption", (selector, expectedValue) => {
    cy.get(selector)
        .children()
        .each(($domElement) => {
            if ($domElement.text().toLowerCase() === expectedValue) {
                const value = $domElement.prop("value");
                cy.get(selector).select(value);
                return;
            }
        });
});

Cypress.Commands.add("selectOption", (selector, expectedValue) => {
    cy.get(selector)
        .children()
        .each(($domElement) => {
            if ($domElement.text().toLowerCase() === expectedValue) {
                const value = $domElement.prop("value");
                cy.get(selector).select(value);
                return;
            }
        });
});
