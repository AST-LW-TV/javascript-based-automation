/// <reference types="cypress" />

// plugins file executes in node environment
// so, if consoled then the output is shown in command line, not in cypress-command logs or browser console

const { downloadFile } = require("cypress-downloadfile/lib/addPlugin");

const fileSystem = require("fs-extra");
const path = require("path");
const mdb = require("../support/db-commons/dbCommands");

const getConfigurationFile = (file) => {
    const pathOfTheConfigFile = path.resolve(
        "cypress",
        "config",
        `${file}.json`
    );
    // handling the file presence
    if (!fileSystem.existsSync(pathOfTheConfigFile)) {
        return {}; // return default cypress.json file
    }
    return fileSystem.readJSON(pathOfTheConfigFile);
};

module.exports = (on, config) => {
    // the tasks are generally performed in node environment ( background )
    on("task", {
        downloadFile,

        getCollectionList(url) {
            return mdb.dbList(url);
        },
        createCollection(info) {
            return mdb.dbCreateCollection(info);
        },
        dropCollection(info) {
            return mdb.dbDropCollection(info);
        },
        createFile(contents) {
            // from task we need to return a promise else throws error...
            return new Promise((resolve, reject) => {
                const absolutePath = path.join(__dirname, "..", "assets");
                contents =
                    "[" +
                    contents
                        .map((element) => {
                            return JSON.stringify(element);
                        })
                        .toString() +
                    "]";
                fileSystem.writeFileSync(
                    absolutePath + "/cypress-official-issues.json",
                    contents,
                    { flag: "w" }
                );
                resolve(true);
            });
        },
    });
    const file = config.env.configFile;
    return getConfigurationFile(file);
};
