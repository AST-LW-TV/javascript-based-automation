/// <reference types="cypress"/>

// this suite has the on end-to-end case 
// checks the presence of db -> creates the collection within db -> drops the collection within db

// the aim of this suite is to use and understand the plugins/index.js file 
// plugins/index.js contains the task events that run in node environment unlike cypress environment

describe("MongoDB Testing", () => {
    beforeEach(() => {
        cy.fixture("db/database-data").as("data");
    });

    it("Validate the presence of the database", () => {
        cy.get("@data").then((data) => {
            cy.task("getCollectionList", Cypress.env("url")).then((res) => {
                // the below is the custom-command
                // location -> cypress-journey/practice-project/cypress/support/mongodb-commons/custom-commands.js
                cy.dbPresence(res, data["database_name"]).then((value) => {
                    expect(value).to.be.true; // validating
                });
            });
        });
    });

    it("Create a collection successfully", () => {
        cy.get("@data").then((data) => {
            // if task requires multiple arguments then use object pattern as it makes things easier
            // instead separate arguments
            cy.task("createCollection", {
                url: Cypress.env("url"),
                dbName: data["new_collection"][0],
                collectionName: data["new_collection"][1],
            }).then((res) => {
                expect(res).to.be.equals(true); // validating
            });
        });
    });

    it("Drop a collection successfully", () => {
        cy.get("@data").then((data) => {
            cy.task("dropCollection", {
                url: Cypress.env("url"),
                dbName: data["drop_collection"][0],
                collectionName: data["drop_collection"][1],
            }).then((res) => {
                expect(res).to.be.true; // validating
            }); 
        });
    });
});
