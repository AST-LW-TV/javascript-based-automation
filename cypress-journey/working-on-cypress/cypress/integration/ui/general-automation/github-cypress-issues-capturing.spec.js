/// <reference types="cypress"/>

import { HomePage } from "../../../support/ui-commons/page-objects/github-website/githubCypressHomePage";

describe("Cypress Github capturing all the issues", () => {
    beforeEach(() => {
        cy.visit("https://github.com/cypress-io/cypress");
    });

    it("Successfully capturing all the cypress issues and created a file in assets directory", () => {
        Cypress.on("uncaught:exception", (err, runnable) => {
            return false;
        });
        // without the above code we encounter the error when performing automation...
        // The following error originated from your application code, not from Cypress. It was caused by an unhandled promise rejection.
        //   > The user aborted a request.
        // When Cypress detects uncaught errors originating from your application it will automatically fail the current test.
        // This behavior is configurable, and you can choose to turn this off by listening to the uncaught:exception event

        HomePage.clickOnIssuesLink();
        HomePage.captureAllIssuesAndNavigateToNextPage();
        HomePage.saveTheResultsInAssets();
    });
});
