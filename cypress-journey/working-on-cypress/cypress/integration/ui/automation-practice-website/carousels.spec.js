/// <reference types="cypress"/>

import homePage from "../../../support/ui-commons/page-objects/automation-practice-website/homePage";

describe(
    "Carousels",
    {
        defaultCommandTimeout: 20000,
    },
    () => {
        beforeEach(() => {
            cy.fixture("ui/ui-data").as("data");
            cy.visit("http://automationpractice.com/index.php");
        });

        it(
            "Validate that the Carousels are rotating after some delay in the home page",
            {
                retries: 10,
            },
            () => {
                cy.get("@data").then((data) => {
                    // actions and assertions
                    homePage.verifyTheCarouselsRotation(data);
                });
            }
        );

        it(
            "Stopping the Carousels rotation when mouse pointer is placed on the image",
            { retries: 10 },
            () => {
                homePage.verifyIfImageIsNotChanged();
            }
        );

        it(
            "Functionality of next button / link on the Carousels",
            {
                retries: 10,
            },
            () => {
                cy.clickButtonAndVerify(".bx-next", true);
            }
        );

        it(
            "Functionality of prev button / link on the Carousels",
            {
                retries: 10,
            },
            () => {
                cy.clickButtonAndVerify(".bx-prev", false);
            }
        );
    }
);
