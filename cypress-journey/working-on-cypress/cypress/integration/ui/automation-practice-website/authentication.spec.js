/// <reference types="cypress"/>

import homePage from "../../../support/ui-commons/page-objects/automation-practice-website/homePage";

// Automation practice website automation
// change the email to new one after executing the test
// the reason to use command timeout of 20sec is this website is very slow
// retries are set to 10 coz this website throws 503 error ... so by retrying the website works again

describe(
    "Authentication",
    {
        defaultCommandTimeout: 20000,
    },
    () => {
        beforeEach(() => {
            cy.fixture("ui/ui-data").as("data");
            cy.visit("http://automationpractice.com/index.php");
        });

        it(
            "Registering the new user / customer",
            {
                retries: 10,
            },
            () => {
                cy.get("@data").then((data) => {
                    // actions
                    const authenticationPage = homePage.clickOnSignInButton();
                    authenticationPage.data = data;
                    authenticationPage.enterEmailAddress(
                        "authenticate_newuser_data",
                        "user_data"
                    );
                    authenticationPage.enterDetailsOfCustomer(
                        "authenticate_newuser_data",
                        "user_data"
                    );
                    authenticationPage.clickOnRegisterButton();
                    // asserting
                    cy.contains("Sign out").should(
                        "have.attr",
                        "title",
                        "Log me out"
                    );
                });
            }
        );

        it(
            "Signing in registered user",
            {
                retries: 10,
            },
            () => {
                cy.get("@data").then((data) => {
                    const email =
                        data["common_data"]["registered_user_data"]["email"];
                    const password =
                        data["common_data"]["registered_user_data"]["password"];
                    // actions
                    const authenticationPage = homePage.clickOnSignInButton();
                    authenticationPage.enterRegisteredUserDetails(
                        email,
                        password
                    );
                    authenticationPage.clickOnSubmitButton();
                    // asserting
                    cy.contains("Sign out").should(
                        "have.attr",
                        "title",
                        "Log me out"
                    );
                });
            }
        );

        it(
            "Signing in with wrong credentials",
            {
                retries: 10,
            },
            () => {
                cy.get("@data").then((data) => {
                    const authenticationPage = homePage.clickOnSignInButton();
                    // actions and asserting
                    cy.enterTheCredentialsAndVerify(
                        authenticationPage,
                        data,
                        "wrong_credentials",
                        [
                            "Authentication failed",
                            "Authentication failed",
                            "Authentication failed",
                        ]
                    );
                });
            }
        );

        it(
            "Missing fields during Signing-in the user",
            {
                retries: 10,
            },
            () => {
                cy.get("@data").then((data) => {
                    const authenticationPage = homePage.clickOnSignInButton();
                    // actions and asserting
                    cy.enterTheCredentialsAndVerify(
                        authenticationPage,
                        data,
                        "missing_credentials",
                        [
                            "Password is required",
                            "An email address required",
                            "An email address required",
                        ]
                    );
                });
            }
        );

        it(
            "Entering the registered email while creating new user / customer",
            {
                retries: 10,
            },
            () => {
                cy.get("@data").then((data) => {
                    // actions
                    const authenticationPage = homePage.clickOnSignInButton();
                    authenticationPage.data = data;
                    authenticationPage.enterEmailAddress(
                        "common_data",
                        "registered_user_data"
                    );
                    authenticationPage.getErrorMsg();
                    // asserting
                    cy.get("@msg").then((msg) => {
                        expect(msg).to.include(
                            "An account using this email address has already been registered. Please enter a valid password or request a new one"
                        );
                    });
                });
            }
        );
    }
);
