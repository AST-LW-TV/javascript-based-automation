/// <reference types="cypress"/>

import categoriesPage from "../../../support/ui-commons/page-objects/automation-practice-website/categoriesPage";
import homePage from "../../../support/ui-commons/page-objects/automation-practice-website/homePage";
import loggedInPage from "../../../support/ui-commons/page-objects/automation-practice-website/loggedInPage";
import orderAndDetailsPage from "../../../support/ui-commons/page-objects/automation-practice-website/orderAndDetailsPage";
import orderPage from "../../../support/ui-commons/page-objects/automation-practice-website/orderPage";

describe("General Functionalities", () => {
    beforeEach(() => {
        cy.fixture("ui/ui-data").as("data");
    });

    it("Validating functionality of logo", { retries: 10 }, () => {
        cy.get("@data").then((data) => {
            // actions
            cy.logInTheUser(data).then(() => {
                loggedInPage.clickOnLogo();
                // asserting
                cy.url().should(
                    "eq",
                    "http://automationpractice.com/index.php"
                );
            });
        });
    });

    it("Displaying product container on hovering", { retries: 10 }, () => {
        cy.hoverVerifyMoveOut();
    });

    it(
        "Add to cart functionality",
        {
            retries: 5,
            defaultCommandTimeout: 20000,
        },
        () => {
            cy.get("@data").then((data) => {
                // actions
                cy.logInTheUser(data);
                loggedInPage.clickOnLogo();
                homePage.addToCart(
                    data["general_functionalities_data"]["product_id"]
                );
                // asserting
                cy.get("@successfulAdditionToCartMsg").then((msg) => {
                    expect(msg.trim()).to.equal(
                        "Product successfully added to your shopping cart"
                    );
                });
            });
        }
    );

    it(
        "Delete product from shopping cart after addition",
        {
            retries: 10,
            defaultCommandTimeout: 20000,
        },
        () => {
            cy.get("@data").then((data) => {
                // assuming no product is added to cart...
                // actions
                cy.addToCart(data, true);
                cy.deleteTheProductFromCart();
                // asserting
                // before deleting the css property display is none but when the cart is empty then display changes to block
                cy.get(
                    homePage.getCartComponent.cartBlockNoProductLocator
                ).should("have.css", "display", "block");
            });
        }
    );

    it(
        "Validating the total cost in the shopping cart",
        {
            retries: 10,
            defaultCommandTimeout: 20000,
        },
        () => {
            cy.get("@data").then((data) => {
                // actions
                cy.addToCart(data, true);
                cy.addToCart(data, false);
                cy.addToCart(data, false);
                homePage.totalPrice();
                // asserting
                cy.get("@totalPrice").then((price) => {
                    expect(price.trim()).to.be.equal(
                        data["general_functionalities_data"]["total_price"]
                    );
                });
            });
        }
    );

    it(
        'Presence of "Proceed to checkout" and "Continue shopping" buttons on the cart layer popup',
        {
            retries: 10,
            defaultCommandTimeout: 20000,
        },
        () => {
            cy.get("@data").then((data) => {
                const productId =
                    data["general_functionalities_data"]["product_id"];
                cy.logInTheUser(data);
                loggedInPage.clickOnLogo();
                cy.clickOnProductAndWaitForPopup(productId).then(() => {
                    // assertions
                    cy.get(homePage.getLayerCartComponent.layerCartLocator).as(
                        "element"
                    );
                    cy.get("@element")
                        .find(
                            homePage.getLayerCartComponent
                                .continueShoppingLocator
                        )
                        .should("be.visible");
                    cy.get("@element")
                        .find(
                            homePage.getLayerCartComponent
                                .proceedToCheckoutLocator
                        )
                        .should("be.visible");
                });
            });
        }
    );

    it(
        "Toggle the View of product container from grid and list format",
        {
            retries: 10,
            defaultCommandTimeout: 20000,
        },
        () => {
            cy.get("@data").then((data) => {
                cy.logInTheUser(data);
                loggedInPage.clickOnLogo();
                homePage.clickOnCategoryLink(
                    homePage.getCategoriesComponent.womenCategory
                );
                // action
                categoriesPage.clickOnList();
                // asserting
                cy.get(categoriesPage.getPageBarComponent.listLocator).should(
                    "have.class",
                    "selected"
                );
                // action
                categoriesPage.clickOnGrid();
                // asserting
                cy.get(categoriesPage.getPageBarComponent.gridLocator).should(
                    "have.class",
                    "selected"
                );
            });
        }
    );

    it(
        "Ordering of the product",
        { retries: 10, defaultCommandTimeout: 20000 },
        () => {
            cy.get("@data").then((data) => {
                const productId =
                    data["general_functionalities_data"]["product_id"];
                // actions
                cy.logInTheUser(data);
                loggedInPage.clickOnLogo();
                cy.clickOnProductAndWaitForPopup(productId).then(() => {
                    cy.get(
                        homePage.getLayerCartComponent.proceedToCheckoutLocator
                    ).click();
                });
                orderPage.clickOnProceedToCheckoutButton();
                orderPage.clickOnAddressConfirmButton();
                orderPage.checkTheTermsOfService();
                orderPage.clickOnCarrierConfirmButton();
                orderPage.payBy("bankWire");
                orderPage.clickOnConfirmOrderButton();
                orderPage.getSuccessMsg();
                // asserting
                cy.get("@successMsg").then((msg) => {
                    expect(msg.trim()).to.contain(
                        "Your order on My Store is complete"
                    );
                });
            });
        }
    );

    it(
        "Downloading the pdf of the invoice after successful ordering of the product",
        {
            retries: 10,
            defaultCommandTimeout: 20000,
        },
        () => {
            // actions
            cy.orderProduct();
            orderPage.navigateToUserPage();
            loggedInPage.clickOnOrderAndHistoryDetailsLink();
            // action and assertion
            orderAndDetailsPage.downloadLatestInvoice();
        }
    );

    it(
        "Add comment to our order",
        { retries: 10, defaultCommandTimeout: 20000 },
        () => {
            // actions
            cy.navigateToOrderDetailsPage();
            orderAndDetailsPage.clickOnTheDetailButton();
            orderAndDetailsPage.addCommentOrMessage();
            orderAndDetailsPage.clickOnSendButton();
            // asserting
            cy.get(
                orderAndDetailsPage.getMessageComponent.successMsgLocator
            ).then(($document) => {
                expect($document.text().trim()).to.contain(
                    "Message successfully sent"
                );
            });
        }
    );
});
