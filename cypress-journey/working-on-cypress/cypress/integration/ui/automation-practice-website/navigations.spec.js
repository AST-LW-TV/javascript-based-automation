/// <reference types="cypress"/>

import homePage from "../../../support/ui-commons/page-objects/automation-practice-website/homePage";
import loggedInPage from "../../../support/ui-commons/page-objects/automation-practice-website/loggedInPage";
import orderAndDetailsPage from "../../../support/ui-commons/page-objects/automation-practice-website/orderAndDetailsPage";
import productPage from "../../../support/ui-commons/page-objects/automation-practice-website/productPage";

describe(
    "Navigation's",
    {
        defaultCommandTimeout: 20000,
    },
    () => {
        beforeEach(() => {
            cy.fixture("ui/ui-data").as("data");
        });

        it(
            "Validating navigation to other page when clicked on various field links present on the logged in page",
            {
                retries: 10,
            },
            () => {
                cy.get("@data").then((data) => {
                    // actions
                    cy.logInTheUser(data).then(() => {
                        // action and assertions
                        loggedInPage.verifyNavigationOfAllFieldsInLoggedPage(
                            data
                        );
                    });
                });
            }
        );

        it(
            "Redirect to information page when clicked on product",
            { retries: 10 },
            () => {
                cy.get("@data").then((data) => {
                    const expectedOutcomes =
                        data["navigations_data"][
                            "product_page_expected_values"
                        ];
                    cy.navigateToHomePage();
                    homePage.clickOnEachProductOnHomePageAndVerifyRedirection(
                        expectedOutcomes
                    );
                });
            }
        );

        it(
            "Redirecting to order page when clicked on the reorder button",
            {
                retries: 10,
                defaultCommandTimeout: 20000,
            },
            () => {
                cy.get("@data").then((data) => {
                    // actions
                    cy.logInTheUser(data);
                    loggedInPage.clickOnOrderAndHistoryDetailsLink();
                    orderAndDetailsPage.clickOnTheReorderButton();
                    // assertions
                    cy.url().should("contain", "controller=order");
                });
            }
        );
    }
);
