/// <reference types="cypress"/>

import homePage from "../../../support/ui-commons/page-objects/automation-practice-website/homePage";
import loggedInPage from "../../../support/ui-commons/page-objects/automation-practice-website/loggedInPage";

describe(
    "Fields display",
    {
        defaultCommandTimeout: 20000,
    },
    () => {
        beforeEach(() => {
            cy.fixture("ui/ui-data").as("data");
        });

        it(
            "Display certain fields once the user logins / signs in",
            {
                retries: 10,
            },
            () => {
                cy.get("@data").then((data) => {
                    // actions
                    cy.logInTheUser(data).then(() => {
                        loggedInPage.captureDisplayedTexts();
                        // asserting
                        cy.get("@displayedTexts").each((text) => {
                            // the method "containing" is not present in standard chai library, refer below ( end of script )
                            expect(
                                data["fields_display_data"][
                                    "logged_in_page_fields"
                                ]
                            ).to.be.containing(text.trim().toLowerCase());
                        });
                    });
                });
            }
        );

        it(
            'Presence of "Home" button in logged in page and redirection to home page on clicking',
            { retries: 10 },
            () => {
                cy.get("@data").then((data) => {
                    // actions
                    cy.logInTheUser(data).then(() => {
                        loggedInPage.clickOnHomeButton();
                    });
                    // asserting
                    cy.url().should(
                        "eq",
                        "http://automationpractice.com/index.php"
                    );
                });
            }
        );

        it(
            "Display of categories on clicking the tab link",
            { retries: 10 },
            () => {
                cy.navigateToHomePage();
                homePage.clickOnCategoriesAndVerifyTheUrl();
            }
        );
    }
);

// the method to add chai-arrays:

// install the package: npm install chai-arrays

// include the following support/index.js
// const assertArrays = require("chai-arrays");
// chai.use(assertArrays);
