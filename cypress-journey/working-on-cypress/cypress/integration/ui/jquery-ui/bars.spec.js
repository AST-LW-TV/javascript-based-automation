/// <reference types="cypress"/>

let targetSlidingValue = "80%";

describe("Different bars in browser", () => {
    it("Sliding bar using html dom element", () => {
        cy.visit("https://jqueryui.com/slider/");
        cy.frameLoaded(".demo-frame");
        cy.iframe()
            .find("#slider > span")
            .then(($domElement) => {
                $domElement.css("left", targetSlidingValue); // can set the CSS properties... WOW!!! never tried before...
            });
    });

    // next task is how we can automate using mouse events
});
