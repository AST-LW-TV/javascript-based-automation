/// <reference types="cypress"/>

let count = 0;

describe("Checkbox and Radio Automation", () => {
    beforeEach(() => {
        cy.visit("https://jqueryui.com/checkboxradio/");
    });

    it("Radio buttons - only Checking is possible", () => {
        cy.frameLoaded(".demo-frame");
        cy.iframe()
            .find("input[type='radio']")
            .each(($domElement) => {
                cy.wrap($domElement)
                    .check({ force: true }) // the reason to use the force: true coz the element is covered by other element
                    .should("be.checked");
            });
    });

    it.only("Checkbox buttons - Checking and Un-Checking", () => {
        cy.frameLoaded(".demo-frame");

        cy.iframe()
            .find("input[type='checkbox']")
            .each(($domElement) => {
                count++;
                // the purpose of count is to select part of the checkboxes instead of all
                // and checking and un-checking only them
                // in this i am selecting only 4 checkboxes instead of 8
                // this may be a scenario to automate ...
                if (count <= 4) {
                    cy.wrap($domElement)
                        .check({ force: true })
                        .should("be.checked");
                } else {
                    count = 0;
                    return;
                }
            });

        cy.iframe()
            .find("input[type='checkbox']")
            .each(($domElement) => {
                count++;
                if (count <= 4) {
                    cy.wrap($domElement)
                        .uncheck({ force: true })
                        .should("not.be.checked");
                } else {
                    return;
                }
            });
    });
});
