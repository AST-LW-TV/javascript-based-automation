/// <reference types="cypress"/>

// Autocomplete - This functionality uses the partial text to produce full text that matches the partial text

// process -

// - type in partial text
// - ul > li will be displayed
// - traverse the li elements to match the target value

let partialText = "a";
let targetValue = "javascript";

describe("Autocomplete Automation", () => {
    it("Select the value using partial text", () => {
        cy.visit("https://jqueryui.com/autocomplete/");
        cy.frameLoaded(".demo-frame");
        cy.iframe()
            .find("#tags")
            .type(partialText)
            .then(() => {
                cy.iframe()
                    .find("#ui-id-1")
                    .children()
                    .each(($domElement) => {
                        const text = $domElement.text().trim();
                        if (text.toLowerCase() === targetValue) {
                            cy.wrap($domElement).click();
                            return;
                        }
                    });
            });
    });
});
