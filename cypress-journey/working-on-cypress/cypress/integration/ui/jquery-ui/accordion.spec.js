/// <reference types="cypress"/>

// Accordion - Displays collapsible content panels for presenting information in a limited amount of space

// This is problem centric - "NOT GENERIC" point to be noted

// process: 

// - first there is going to be default selected one 
// - now verify the default one is active and rest others are not-active 
// - now randomly select other element and click on it 
// - then verify the active one and verify others are in-active 
// - repeat the process for finite number of counts

let idNumber = 1;
let min = 1;
let max = null;
let remainingIds = null;
let count = 0;

// this function is used for selecting random element from the array to click on it
function selectRandomElementWithinArray(array) {
    const randomIndex = Math.floor(
        Math.random() * (array.length - 1 - 0 + 1) + 0
    );
    return array[randomIndex];
}

// here, the sequence of array is produced
// if there are 4 target elements
// then this function produces = [3,5,7] 
// h3 - 1 
//  - div - 2 
// h3 - 3
//  - div - 4
// .... 
// here, want only h3 elements so returning odd number from the sequence  
function findRemainingNumbersApartFromSelectedId(min, max, selectedValue) {
    const temp = [];
    for (let i = min; i <= max * 2; i++) {
        if (i !== selectedValue && i % 2 !== 0) temp.push(i);
    }
    return temp;
}

describe("Accordion Automation", () => {
    beforeEach(() => {});

    it("Validating whether the collapsible content is active or not", () => {
        cy.visit("https://jqueryui.com/accordion/");
        cy.frameLoaded(".demo-frame");

        // check if i press on header 1 then next div is active
        // then click on some random header 2 then check if next div is active and also check if prev div is inactive
        // repeat this some number of times

        // here, checking how many header elements are present and storing them in max variable 
        // so that we can use that to produce the remaining sequence numbers
        cy.iframe()
            .find("h3[id^='ui-id-']")
            .then(($domElements) => {
                max = $domElements.length;
            });

        function clickAndVerify() {
            // checking the active element
            cy.iframe()
                .find(`#ui-id-${idNumber}`)
                .should("have.attr", "aria-selected", "true")
                .then(() => {
                    return findRemainingNumbersApartFromSelectedId(
                        min,
                        max,
                        idNumber
                    );
                })
                .then((ids) => {
                    remainingIds = ids;
                    cy.wrap(ids).each((id) => {
                        // checking the non-active elements
                        cy.iframe()
                            .find(`h3[id^='ui-id-${id}']`)
                            .should("have.attr", "aria-selected", "false");
                    });
                })
                .then(() => {
                    idNumber = selectRandomElementWithinArray(remainingIds);
                    // after that click on some random element
                    cy.iframe().find(`#ui-id-${idNumber}`).click();
                })
                .then(() => {
                    if (count != 4) {
                        // repeat some finite count times
                        clickAndVerify();
                        count++;
                    } else {
                        return;
                    }
                });
        }
        // calling the function
        clickAndVerify();
    });
});
