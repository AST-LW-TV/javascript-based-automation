/// <reference types="cypress"/>

// the following example consists the concept of test retries
// sometimes test fails as no element is found, but retrying the same test again works...

// process:

// - first check the year
//   - if the year is less than the expected then click prev button
//   - if the year ia greater than the expected then click on the next button
// - after the year matches then select month
// - now click on the next or prev button until it matches the expected month
// - once month is matched, now click on the specified date by traversing through the calendar table

let date = new Date();

describe("Date Picker Automation", () => {
    beforeEach(() => {});

    it(
        "Select a date from a popup or inline calendar",
        {
            retries: 3,
        },
        () => {
            let referenceYear = date.getFullYear(); // to get current year for matching month either to press next or prev button
            cy.visit("https://jqueryui.com/datepicker/#default");
            cy.frameLoaded("iframe[class='demo-frame']");
            cy.iframe().then(($domElement) => {
                cy.iframe().find("#datepicker").click(); // focusing on the input element it can be done by either clicking on it or focusing on it

                // need to be specify the date, month, year ( this can be present in fixtures folder )
                const dateToBeSelected = "15";
                const monthToBeSelected = "september";
                const yearToBeSelected = "2015";

                // selecting the year
                function yearSelection() {
                    cy.iframe()
                        .find("span[class='ui-datepicker-year']")
                        .then(($domElement) => {
                            const year = $domElement.text();
                            if (
                                Number(yearToBeSelected) !== year &&
                                Number(yearToBeSelected) < year
                            ) {
                                cy.iframe().find("a[title='Prev']").click();
                            } else if (
                                Number(yearToBeSelected) !== year &&
                                Number(yearToBeSelected) > year
                            ) {
                                cy.iframe().find("a[title='Next']").click();
                            } else {
                                return;
                            }
                            yearSelection();
                        });
                }

                // selecting month
                function monthSelection() {
                    // year is for reference, either to press next or prev button for month selection
                    let year = null;
                    cy.iframe()
                        .find("span[class='ui-datepicker-year']")
                        .then(($domElement) => {
                            year = $domElement.text();
                        });
                    cy.iframe()
                        .find("span[class='ui-datepicker-month']")
                        .then(($domElement) => {
                            const month = $domElement
                                .text()
                                .trim()
                                .toLowerCase();
                            if (monthToBeSelected !== month) {
                                // 1998 < 2021
                                // december - prev
                                if (Number(yearToBeSelected) < referenceYear) {
                                    cy.iframe().find("a[title='Prev']").click();
                                    monthSelection();
                                }
                                // 2032 > 2021
                                // january - next
                                else if (
                                    Number(yearToBeSelected) > referenceYear
                                ) {
                                    cy.iframe().find("a[title='Next']").click();
                                    monthSelection();
                                }
                            } else {
                                return;
                            }
                        });
                }

                // selecting date
                function daySelection() {
                    cy.iframe()
                        .find(".ui-state-default")
                        .each(($domElement) => {
                            const innerDateValue = $domElement.text();
                            if (innerDateValue === dateToBeSelected) {
                                cy.wrap($domElement).click();
                            }
                        });
                }

                // performing actions
                yearSelection();
                monthSelection();
                daySelection();
            });
        }
    );
});
