/// <reference types="cypress"/>

describe("Mouse Event Automation", () => {
    it("Drag and drop Event", () => {
        cy.visit("https://www.globalsqa.com/demo-site/draganddrop/");
        // when there are more than one iframe to be selected then cypress-iframe throws error
        // the workaround is to take get the iframe specified and then in contents find "body" and wrap with cy.wrap()
        // to use cypress commands on it
        cy.get(".demo-frame")
            // here three iframes were there, from which i wanted first one...
            .eq(0)
            .then(($domElement) => {
                const body = $domElement.contents().find("body");
                // now the iframe is locked under insideIFrame as alias name
                cy.wrap(body).as("insideIFrame");
                cy.get("@insideIFrame")
                    .find("#gallery")
                    .children()
                    .each(($domElement) => {
                        // action to be performed is,
                        // cypress language -
                        //  - mousedown on the element -> mousemove to specified location -> then mouseup
                        // human understanding -
                        //  - pressing the mouse button -> moving the mouse cursor        -> lifting the mouse button
                        cy.wrap($domElement)
                            .trigger("mousedown", { which: 1 })
                            // the element was covered so just forcing the action to be performed
                            // used "coordinates extension" link - https://chrome.google.com/webstore/detail/coordinates/bpflbjmbfccblbhlcmlgkajdpoiepmkd
                            // to find the location to be dropped
                            .trigger("mousemove", 600, 200, { force: true })
                            .trigger("mouseup");
                    });
                // validate if all the elements are dragged and dropped inside another element
                cy.get("@insideIFrame")
                    .find("#trash")
                    .children()
                    .eq(1)
                    .children()
                    .should("have.length", 4); // here 4 is hardcoded but it can be eliminated ...
                // the above is the good example of FLAKY TESTS, same code same conditions but different results pass sometimes and fail another times
                // issues: 
                // - sometimes the element is not identified
                // - sometimes only three elements were dropped
                // need to figure out...
            });
    });
});
