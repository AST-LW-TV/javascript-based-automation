/// <reference types="cypress"/>

// official documentation - https://docs.cypress.io/api/commands/intercept

/*

XMLHttpRequest is an API takes form of an object whose methods are used top transfer data 
between a browser sending requests and the web server serving responses
XMLHttpRequest API can be used to update the browser page without reloading the page, request and receive 
server data after a page loads and data is send in the background

Stubbing can be used to avoid actual responses from the server and instead create our own "fake" responses 
that interpret as actual responses

Advantages of stubbing - 
- having control over the body, headers and status of the responses 
- quick response times of the server
- no required code changes to the server 
- network delay simulations can be added to the request 

Disadvantages of stubbing requests
- inability to have test coverage on some server endpoints 
- no guarantee that the response data and stubbed data are a match

*/

describe("Intercept Concepts", () => {
    // what is done ?
    // -> actually we visited the website "https://jsonplaceholder.typicode.com/"
    // -> clicked on the hyper link having CSS selector "a[href='/users']"
    // -> then the page renders the response
    // but we are spying on "https://jsonplaceholder.typicode.com/users" if we find any changes like making a request
    // to backend then it gets captured and we can perform the validations on the received response
    // to see the response either use cy.log() or console.log() [ console.log outputs in browser console ]
    it("Spying", () => {
        cy.visit("https://jsonplaceholder.typicode.com/");
        // by using "path", the resource path will be concatenated with super domain
        // https://jsonplaceholder.typicode.com/ - super domain
        // /users - resource path
        // after concatenation we have "https://jsonplaceholder.typicode.com/users"
        // else
        // we can use url option - complete path to be given
        cy.intercept({
            method: "GET",
            path: "/users",
        }).as("users");
        cy.get("a[href='/users']").click(); // note apply chromeWebSecurity:false in cypress.json file to avoid CORS error
        // the use of "wait" here is to wait for the successful response
        cy.wait("@users").then((res) => {
            const body = res.response.body;
            expect(body).to.be.length(10);
        });
    });

    // what if change the returned response to favour our conditions ?
    // this is called stubbing
    // in the following snippet lets try to modify the status code to 300 and body to have only {"response":"modified"}
    it("Stubbing", () => {
        cy.visit("https://jsonplaceholder.typicode.com/");
        cy.intercept(
            {
                method: "GET",
                path: "/users",
            },
            {
                statusCode: 300,
                body: {
                    response: "modified",
                },
            }
        ).as("users");
        cy.get("a[href='/users']").click();
        cy.wait("@users").then((res) => {
            const body = res.response.body;
            console.log(res);
            expect(body).to.have.property("response", "modified"); // validating the changed response
        });
    });

    // we can pass the static response to the intercept
    // also this can be stored in fixture and use as - {fixtures:< file name where the static response is stored >}
    const staticResponse = {
        statusCode: 400,
        headers: {
            "Content-Type": "text/html",
        },
        body: {
            response: "hacked",
        },
    };

    it("Stubbing using static responses", () => {
        cy.visit("https://jsonplaceholder.typicode.com/");
        cy.intercept(
            {
                path: "/comments",
                method: "GET",
            },
            staticResponse
        ).as("comments");
        cy.get("a[href='/comments']").click();
        cy.wait("@comments").then((res) => {
            const stubbedBody = res.response.body;
            const stubbedStatusCode = res.response.statusCode;
            const stubbedHeaders = res.response.headers;
            // in stubbed response we manually changed the response body to be of text/html instead of application/json
            // expect().to.have.property() - will fail
            expect(stubbedBody).to.have.equal('{"response":"hacked"}');
            expect(stubbedStatusCode).to.equal(400); // validating the status code
            expect(stubbedHeaders).to.have.property(
                "Content-Type",
                "text/html"
            ); // validating the headers to have stubbed property
        });
    });

    it("Stubbing with delay in response", () => {
        let startTime,
            endTime = [null, null];

        cy.visit("https://jsonplaceholder.typicode.com/");
        cy.intercept(
            {
                path: "/photos",
                method: "GET",
            },
            { delay: 8000 }
        ).as("photos");
        cy.get("a[href='/photos']").click();
        startTime = new Date();
        cy.wait("@photos", { defaultTimeout: 10000 }).then((res) => {
            endTime = new Date();
            const responseTime =
                (endTime.getTime() - startTime.getTime()) / 1000;
            expect(responseTime).to.be.greaterThan(8); // validating if we really introduced the delay...
        });
        // we can also manipulate speed of data transfer and or break the browser connection
        // using throttleKbps or forceNetworkError options
    });

    // route handler
    it("Accessing the request internally before sending to server and modifying", () => {
        cy.visit("https://jsonplaceholder.typicode.com/");
        cy.intercept(
            {
                method: "GET",
                path: "/users",
            },
            (req) => {
                req.method = "POST";
            }
        ).as("users");
        cy.get("a[href='/users']").click();
        cy.wait("@users").then((res) => {
            const statusCode = res.response.statusCode;
            expect(statusCode).to.be.equal(201); // validating the status code
            // actually the request was GET method, but now its changed to POST
            // so now the resource will be created and status code will be 201 ( created resource )
        });
    });

    // request events
    it("Request events", () => {
        cy.visit("https://jsonplaceholder.typicode.com/");
        cy.intercept(
            {
                method: "GET",
                path: "/users",
            },
            (req) => {
                req.on("before:response", (res) => {
                    console.log("Before sending the response to backend");
                });
                req.on("response", (req) => {
                    console.log(
                        "Just Before sending the request to the browser"
                    );
                });
                req.on("after:response", (req) => {
                    console.log("After sending the response from the backend");
                });
                // we also have
                // - res.reply() - modify the request
                // - res.continue() - send the request to backend
                // - res.destroy() - destroy the request
                // - res.send() - similar to reply
            }
        ).as("users");
        cy.get("a[href='/users']").click();
        cy.wait("@users");
    });

    // the real purpose of stubbing is test alternate response or edge test cases that can behave
    // differently from actual server response
    // intercept() is used to handle XHR requests made for backend

    // Question
    // does intercept be used for standalone api's without any event is raised ?
});
