/// <reference types="cypress"/>

//                         cookies              local storage   session storage
// capacity                4kb                  10mb            5mb
// browsers                HTML4/HTML5          HTML5           HTML5
// accessible from         any window           any window      same tab
// expires                 manually set         never           on tab close
// storage location        browser and server   browser only    browser only
// sent with requests      yes                  no              no

describe("Storages in Browser", () => {
    // Local Storage - used to save key-value pairs in the web browser without any expiration date

    // // setting the key-value pair in the web browser
    // const name = "John";
    // const email = "john@gmail.com";
    // it("Validate the local storage values", () => {
    //     cy.visit("https://www.google.com").then(() => {
    //         window.localStorage.setItem("name", name);
    //         window.localStorage.setItem("email", email);
    //         const nameFromLocalStorage = window.localStorage.getItem("name");
    //         const emailFromLocalStorage = window.localStorage.getItem("email");
    //         expect(nameFromLocalStorage).to.be.equals(name);
    //         expect(emailFromLocalStorage).to.be.equals(email);
    //     });
    // });

    // // Generally in cypress the local storage values are erased after each test execution
    // it("Try to use the above local storage values in this test", () => {
    //     cy.visit("https://www.google.com").then(() => {
    //         const value = window.localStorage.getItem("name"); // the "name" value will return null
    //         expect(value).to.be.equals(null);
    //     });
    // });

    // toggle the commenting between lines 14-35 and ... to see alternate output on the cypress runner
    // the solution is to use before or beforeEach hook to set the local storage values

    // this data can be saved under fixture 
    const name = "John";
    const email = "john@gmail.com";
    beforeEach(() => {
        window.localStorage.setItem("name", name);
        window.localStorage.setItem("email", email);
    });

    it("Using beforeEach hook to read local storage values - Test 1", () => {
        expect(window.localStorage.getItem("name")).to.be.equals(name);
    });
    // now any number of tests can access the storage values coz the hook runs before the test is executed 

    // Session Storage - the concept of session storage is same as the local storage but it expires on closing of the tab
    //                   whereas, the local storage retains the contents even when the window or tab is closed\
    // we can set, get and remove the session storage values using the following manner, 
    // window.sessionStorage.setItem(<key>,<value>); 
    // window.sessionStorage.getItem(<key>); 
    // window.sessionStorage.removeItem(<key>);
});
