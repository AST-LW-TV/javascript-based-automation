/// <reference types="cypress"/>

describe("Authentication Testing", () => {
    let authenticationToken = null;

    beforeEach(() => {
        cy.fixture("api/tmdb-api-data/api-data").as("data");
    });

    it("Creating a Guest Session", () => {
        cy.request({
            url: "https://api.themoviedb.org/3/authentication/guest_session/new",
            method: "GET",
            qs: {
                api_key: Cypress.env("api_key"),
            },
        }).then(response=>{
            const id=response.body["guest_session_id"];
            expect(id).not.to.be.null;
        });
    });

    it("Successful generation of token for Authentication", () => {
        cy.request({
            url: "https://api.themoviedb.org/3/authentication/token/new",
            method: "GET",
            qs: {
                api_key: Cypress.env("api_key"),
            },
        }).as("response");
        cy.get("@response").then((response) => {
            const successMsg = response.body["success"];
            authenticationToken = response.body["request_token"];
            expect(successMsg).to.be.true;
        });
    });

    it("Creating Session Successfully", () => {
        cy.get("@data").then((data) => {
            cy.request({
                url: "https://api.themoviedb.org/3/authentication/token/validate_with_login",
                method: "POST",
                qs: {
                    api_key: Cypress.env("api_key"),
                },
                body: {
                    username: data["username"],
                    password: data["password"],
                    request_token: authenticationToken,
                },
            }).as("response");
        });
        cy.get("@response").then((response) => {
            const successMsg = response.body["success"];
            expect(successMsg).to.be.true;
        });
    });

    // negative test case
    // it fails
    // glitch in API not from our side
    it("Deleting the created session", () => {
        cy.get("@data").then((data) => {
            cy.request({
                url: "https://api.themoviedb.org/3/authentication/session",
                method: "DELETE",
                qs: {
                    api_key: Cypress.env("api_key"),
                },
                body: {
                    session_id: authenticationToken,
                },
            }).as("response");
        });
        cy.get("@response").then((response) => {
            const successMsg = response.body["success"];
            expect(successMsg).to.be.true;
        });
    });
});
