/// <reference types="cypress"/>

// https://developers.themoviedb.org/3/movies/get-movie-details

describe("Movies API testing", () => {
    before(() => {
        cy.fixture("api/tmdb-api-data/api-data").as("data");
    });

    it("Validate status code", () => {
        cy.get("@data").then((data) => {
            cy.request({
                url: `https://api.themoviedb.org/3/movie/${data["movie_id"]}`, // accessing the data from fixtures
                method: "GET",
                qs: {
                    api_key: Cypress.env("api_key"),
                },
            }).as("getMovieUsingId");
        });

        cy.get("@getMovieUsingId").then((response) => {
            expect(response.status).to.be.equal(200);
        });
    });
});
