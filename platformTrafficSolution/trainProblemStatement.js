// conditions,
// - list of arrival and departure are always sorted
// - number of arrival is always equal to number of departures
// - train that arrives first leaves first

// Approach to solution,
// - first to create the structure of the platform
// {
//     platformNumber:1,
//     isEmpty:<boolean value>,
//     arrival:<arrival time>
//     departure:<departure time>
// }
// - initiate new platform for the first incoming train
// - then compare the previous departure time and next arrival time if less, then modify platform properties
// - if no platform is empty then create a new platform
// - at the end of the procedure minimum number of platforms are equal to number of created platforms during the procedure

// output,
// When First train arrives:  [
//   { platformNumber: 1, isEmpty: false, arrival: 1300, departure: 1330 }
// ]
// Changes to current platforms:  [
//   { platformNumber: 1, isEmpty: true, arrival: 1400, departure: 1410 }
// ]
// Changes to current platforms:  [
//   { platformNumber: 1, isEmpty: false, arrival: 1530, departure: 1800 }
// ]
// New platform creation:  [
//   { platformNumber: 1, isEmpty: false, arrival: 1530, departure: 1800 },
//   { platformNumber: 2, isEmpty: false, arrival: 1700, departure: 1830 }
// ]
// Minimum number of platform required to cater the railway traffic: 2

const TrainTimes = Object.freeze({
    arrival: [1300, 1400, 1530, 1700],
    departure: [1330, 1410, 1800, 1830],
});

class Platform {
    constructor() {
        this.platformNumber = 1;
        this.platforms = [];
    }
    // creates new platform
    newPlatform(arrival, departure) {
        this.platforms.push({
            platformNumber: this.platformNumber,
            isEmpty: false,
            arrival: arrival,
            departure: departure,
        });
        this.platformNumber += 1;
    }
    // inverts the platform state when the train leaves the station
    changePlatformState(platform) {
        platform.isEmpty = !platform.isEmpty;
    }
    // checks whether there is an empty platform when train arrives at the station
    checkForFreePlatform(arrivalTimeOfNextTrain) {
        let freePlatform = null;
        for (let platform of this.platforms) {
            if (platform.departure < arrivalTimeOfNextTrain) {
                this.changePlatformState(platform);
                platform.arrival = arrivalTimeOfNextTrain;
                freePlatform = platform;
                break;
            }
        }
        return freePlatform;
    }
}

class Train {
    constructor() {
        this.platform = new Platform();
    }
    logic() {
        this.platform.newPlatform(
            TrainTimes.arrival[0],
            TrainTimes.departure[0]
        );
        console.log("When First train arrives: ", this.platform.platforms);
        for (let index = 1; index < TrainTimes.arrival.length; index++) {
            let currentPlatform = this.platform.checkForFreePlatform(
                TrainTimes.arrival[index]
            );
            if (currentPlatform === null) {
                this.platform.newPlatform(
                    TrainTimes.arrival[index],
                    TrainTimes.departure[index]
                );
                console.log("New platform creation: ", this.platform.platforms);
            } else {
                currentPlatform.departure = TrainTimes.departure[index];
                console.log(
                    "Changes to current platforms: ",
                    this.platform.platforms
                );
            }
        }
    }
    toString() {
        return this.platform.platforms.length;
    }
}

const train = new Train();
train.logic();
console.log(
    "Minimum number of platform required to cater the railway traffic: " +
        train.toString()
);
